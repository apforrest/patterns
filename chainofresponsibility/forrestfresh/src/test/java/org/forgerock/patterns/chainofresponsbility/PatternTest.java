package org.forgerock.patterns.chainofresponsbility;

import org.forgerock.patterns.chainofresponsibility.Handler;
import org.forgerock.patterns.chainofresponsibility.HandlerFour;
import org.forgerock.patterns.chainofresponsibility.HandlerOne;
import org.forgerock.patterns.chainofresponsibility.HandlerThree;
import org.forgerock.patterns.chainofresponsibility.HandlerTwo;
import org.forgerock.patterns.chainofresponsibility.NumberRequest;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import static org.fest.assertions.Assertions.assertThat;

/**
 */
public class PatternTest {

    private Handler handler;

    @BeforeMethod
    public void setUp() {
        handler = HandlerOne.getInstance()
                .setSuccessor(HandlerTwo.getInstance()
                        .setSuccessor(HandlerThree.getInstance()
                                .setSuccessor(HandlerFour.getInstance())));
    }


    @Test
    public void testOne() {
        assertThat(handler.handleRequest(NumberRequest.getInstance(1))).isEqualTo("one");
    }

    @Test
    public void testTwo() {
        assertThat(handler.handleRequest(NumberRequest.getInstance(2))).isEqualTo("two");
    }

    @Test
    public void testThree() {
        assertThat(handler.handleRequest(NumberRequest.getInstance(3))).isEqualTo("three");
    }

    @Test
    public void testFour() {
        assertThat(handler.handleRequest(NumberRequest.getInstance(4))).isEqualTo("Unknown value 4");
    }

}

