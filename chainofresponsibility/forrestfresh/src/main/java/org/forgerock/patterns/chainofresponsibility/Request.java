package org.forgerock.patterns.chainofresponsibility;

/**
 */
public interface Request {

    int getValue();
}
