package org.forgerock.patterns.chainofresponsibility;

/**
 */
public class HandlerTwo extends BaseHandler {

    private HandlerTwo() {
    }

    @Override
    public String handleRequest(Request request) {
        if (request.getValue() == 2) {
            return "two";
        }

        return passOnRequest(request);
    }

    public static Handler getInstance() {
        return new HandlerTwo();
    }

}
