package org.forgerock.patterns.chainofresponsibility;

/**
 */
public interface Handler {

    String handleRequest(Request request);

    Handler setSuccessor(final Handler successor);

}
