package org.forgerock.patterns.chainofresponsibility;

/**
 */
public class HandlerOne extends BaseHandler {

    private HandlerOne() {
    }

    @Override
    public String handleRequest(Request request) {
        if (request.getValue() == 1) {
            return "one";
        }

        return passOnRequest(request);
    }

    public static Handler getInstance() {
        return new HandlerOne();
    }

}
