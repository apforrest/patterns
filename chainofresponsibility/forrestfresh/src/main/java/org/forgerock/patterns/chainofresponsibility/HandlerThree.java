package org.forgerock.patterns.chainofresponsibility;

/**
 */
public class HandlerThree extends BaseHandler {

    private HandlerThree() {
    }

    @Override
    public String handleRequest(Request request) {
        if (request.getValue() == 3) {
            return "three";
        }

        return passOnRequest(request);
    }

    public static Handler getInstance() {
        return new HandlerThree();
    }

}
