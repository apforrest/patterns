package org.forgerock.patterns.chainofresponsibility;

/**
 */
public class NumberRequest implements Request {

    private final int value;

    private NumberRequest(final int value) {
        this.value = value;
    }

    @Override
    public int getValue() {
        return value;
    }

    public static Request getInstance(final int value) {
        return new NumberRequest(value);
    }

}
