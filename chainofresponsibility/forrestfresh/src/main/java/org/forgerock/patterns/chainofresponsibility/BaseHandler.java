package org.forgerock.patterns.chainofresponsibility;

/**
 */
public abstract class BaseHandler implements Handler {

    private Handler successor;

    public Handler setSuccessor(final Handler successor) {
        this.successor = successor;
        return this;
    }

    protected String passOnRequest(Request request) {
        return successor.handleRequest(request);
    }

}
