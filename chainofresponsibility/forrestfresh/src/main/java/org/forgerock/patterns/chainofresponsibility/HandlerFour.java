package org.forgerock.patterns.chainofresponsibility;

/**
 */
public class HandlerFour extends BaseHandler {

    private HandlerFour() {
    }

    @Override
    public String handleRequest(Request request) {
        return "Unknown value " + request.getValue();
    }

    public static Handler getInstance() {
        return new HandlerFour();
    }

}
