package org.forgerock.visitor.forrestfresh;

import org.testng.annotations.Test;

/**
 */
public class VisitorTest {

    @Test
    public void printReport() {
        Item tinBeans = new Item("Tin beans");
        Item chicken = new Item("Chicken");
        Item cake = new Item("Cake");
        Item eggs = new Item("Eggs");
        Item chocolate = new Item("Chocolate");

        Order firstOrder = new Order("First order");
        firstOrder.addItem(tinBeans);
        firstOrder.addItem(cake);
        firstOrder.addItem(chocolate);

        Order secondOrder = new Order("Second order");
        secondOrder.addItem(chicken);
        secondOrder.addItem(eggs);

        Customer james = new Customer("James");
        james.addOrder(firstOrder);
        james.addOrder(secondOrder);

        Customer andrew = new Customer("Andrew");
        andrew.addOrder(secondOrder);

        CustomerGroup group = new CustomerGroup();
        group.addCustomer(james);
        group.addCustomer(andrew);

        GeneralReport report = new GeneralReport();
        group.accept(report);

        System.out.println(report.displayResults());
    }

}
