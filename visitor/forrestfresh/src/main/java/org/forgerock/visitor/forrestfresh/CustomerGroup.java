package org.forgerock.visitor.forrestfresh;

import java.util.ArrayList;
import java.util.List;

/**
 */
public class CustomerGroup implements Visitable {

    private final List<Customer> customers;

    public CustomerGroup() {
        customers = new ArrayList<>();
    }

    @Override
    public void accept(Visitor visitor) {
        customers.forEach(c -> c.accept(visitor));
    }

    public void addCustomer(Customer customer) {
        customers.add(customer);
    }

}
