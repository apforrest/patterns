package org.forgerock.visitor.forrestfresh;

import java.util.ArrayList;
import java.util.List;

/**
 */
public class Customer implements Visitable {

    private final String name;
    private final List<Order> orders;

    public Customer(final String name) {
        this.name = name;
        orders = new ArrayList<>();
    }

    @Override
    public void accept(Visitor visitor) {
        visitor.visit(this);
        orders.forEach(o -> o.accept(visitor));
    }

    public String getName() {
        return name;
    }

    public void addOrder(Order order) {
        orders.add(order);
    }

}
