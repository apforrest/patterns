package org.forgerock.visitor.forrestfresh;

/**
 */
public interface Visitable {

    void accept(Visitor visitor);

}
