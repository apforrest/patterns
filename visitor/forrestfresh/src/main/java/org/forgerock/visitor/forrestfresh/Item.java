package org.forgerock.visitor.forrestfresh;

/**
 */
public class Item implements Visitable {

    private final String name;

    public Item(final String name) {
        this.name = name;
    }

    public String getName() {
        return name;
    }

    @Override
    public void accept(Visitor visitor) {
        visitor.visit(this);
    }
}
