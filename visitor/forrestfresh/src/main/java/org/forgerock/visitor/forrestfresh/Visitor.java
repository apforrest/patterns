package org.forgerock.visitor.forrestfresh;

/**
 */
public interface Visitor {

    void visit(Customer customer);

    void visit(Order order);

    void visit(Item item);

}
