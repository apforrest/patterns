package org.forgerock.visitor.forrestfresh;

import java.util.ArrayList;
import java.util.List;

/**
 */
public class Order implements Visitable {

    private final String name;
    private final List<Item> items;

    public Order(final String name) {
        this.name = name;
        items = new ArrayList<>();
    }

    public String getName() {
        return name;
    }

    public void addItem(Item item) {
        items.add(item);
    }

    @Override
    public void accept(Visitor visitor) {
        visitor.visit(this);
        items.forEach(o -> o.accept(visitor));
    }

}
