package org.forgerock.visitor.forrestfresh;

/**
 */
public class GeneralReport implements Visitor {

    private final StringBuilder builder;

    public GeneralReport() {
        builder = new StringBuilder();
    }

    @Override
    public void visit(Customer customer) {
        builder
                .append("Customer ")
                .append(customer.getName())
                .append(" has orders:")
                .append('\n');
    }

    @Override
    public void visit(Order order) {
        builder
                .append('\t')
                .append(order.getName())
                .append(" which items: ")
                .append('\n');
    }

    @Override
    public void visit(Item item) {
        builder
                .append('\t')
                .append('\t')
                .append(item.getName())
                .append('\n');
    }

    public String displayResults() {
        return builder.toString();
    }

}
