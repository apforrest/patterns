package org.forgerock.visitor.mcdonnell.doubledispatch;

import org.testng.annotations.Test;

import java.util.ArrayList;
import java.util.List;

import static org.fest.assertions.Assertions.assertThat;

public class DoubleDispatchTest {

    @Test
    public void javaImplementsDynamicDispatch() {

        /**
         * Java uses the runtime type of the object on which a method is called to identify
         * which implementation of a potentially overridden method gets called.
         */

        // Given
        Sprite sprite = new Sprite();
        Sprite asteroid = new Asteroid();
        Sprite spaceShip = new SpaceShip();

        // When
        String spriteType = sprite.getType();
        String asteroidType = asteroid.getType();
        String spaceShipType = spaceShip.getType();

        // Then
        assertThat(spriteType).isEqualTo("Sprite");
        assertThat(asteroidType).isEqualTo("Asteroid");
        assertThat(spaceShipType).isEqualTo("SpaceShip");

        /**
         * Even though the compiler thinks both variables are Sprites, the call to getType
         * is bound (at runtime) to the most specific implementation.
         */
    }

    @Test
    public void javaDoesNotImplementMultipleDispatch() {

        /**
         * Java does not support multiple dispatch (aka multi-methods). This means that,
         * when an overloaded method is called, the compile-time types of the arguments
         * dictate which signature of the method is invoked.
         */

        // Given
        Sprite sprite = new Sprite();
        Sprite asteroid = new Asteroid();
        Sprite spaceShip = new SpaceShip();
        DescribingVisitor describingVisitor = new DescribingVisitor();

        // When
        describingVisitor.visit(sprite);
        describingVisitor.visit(asteroid);
        describingVisitor.visit(spaceShip);

        // Then
        assertThat(describingVisitor.descriptions).containsExactly(
                "I see a Sprite", "I see a Sprite", "I see a Sprite");

        /**
         * The compiler thinks all the variables are Sprites, and therefore all visit
         * calls are bound to the implementation that accepts Sprite - Even if the
         * runtime type of the passed object is actually an Asteroid or SpaceShip.
         */
    }

    @Test
    public void theVisitorPatternImplementsDoubleDispatch() {

        /**
         * By forcing the visited object to pass itself back to the visitor,
         * we introduce double-dispatch. This means that the method called to
         * operate on the visited object is dependent on both the type of the
         * visitor and also the type of the visited object.
         */

        // Given
        Sprite sprite = new Sprite();
        Sprite asteroid = new Asteroid();
        Sprite spaceShip = new SpaceShip();
        DescribingVisitor describingVisitor = new DescribingVisitor();

        // When
        sprite.accept(describingVisitor);
        asteroid.accept(describingVisitor);
        spaceShip.accept(describingVisitor);

        // Then
        assertThat(describingVisitor.descriptions).containsExactly(
                "I see a Sprite", "I see an Asteroid", "I see a SpaceShip");

        /**
         * Yay! Double-Dispatch FTW!
         */
    }

    class Sprite {
        public void accept(Visitor visitor) {
            visitor.visit(this);
        }
        public String getType() {
            return "Sprite";
        }
    }

    class Asteroid extends Sprite {
        @Override
        public void accept(Visitor visitor) {
            visitor.visit(this);
        }
        @Override
        public String getType() {
            return "Asteroid";
        }
    }

    class SpaceShip extends Sprite {
        @Override
        public void accept(Visitor visitor) {
            visitor.visit(this);
        }
        @Override
        public String getType() {
            return "SpaceShip";
        }
    }

    interface Visitor {
        void visit(Sprite sprite);
        void visit(Asteroid asteroid);
        void visit(SpaceShip spaceShip);
    }

    class DescribingVisitor implements Visitor {

        List<String> descriptions = new ArrayList<>();

        @Override
        public void visit(Sprite sprite) {
            descriptions.add("I see a Sprite");
        }

        @Override
        public void visit(SpaceShip spaceShip) {
            descriptions.add("I see a SpaceShip");
        }

        @Override
        public void visit(Asteroid asteroid) {
            descriptions.add("I see an Asteroid");
        }
    }
}
